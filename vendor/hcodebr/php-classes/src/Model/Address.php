<?php 

namespace Hcode\DB;
namespace Hcode\Model;


use Hcode\DB\Sql;
use Hcode\Model;
use Hcode\Model\User;

class Address extends Model{

	const SESSION_ERROR = "AddressError";

	public static function getCEP($nrocep){
		$nrocep = str_replace("-", "", $nrocep);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://viacep.com.br/ws/$nrocep/json/");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$data = json_decode(curl_exec($ch),true);

		curl_close($ch);

		//echo $response

		return $data;
	}

	public function loadFromCEP($nrocep){
		$data = Address::getCEP($nrocep);

		if (isset($data['logradouro']) && $data['logradouro']) {
			
			$this->setdesaddress($data['logradouro']);
			$this->setdescomplement($data['complemento']);
			$this->setdesdistrict($data['bairro']);
			$this->setdescity($data['localidade']);
			$this->setdesstate($data['uf']);
			$this->setdescountry('Brasil');
			$this->setdeszipcode($nrocep);
		}
	}

	public function save(){

		$sql = new Sql();
		/*
			pidaddress int(11), 
			pidperson int(11),
			pdesaddress varchar(128),
			pdescomplement varchar(32),
			pdescity varchar(32),
			pdesstate varchar(32),
			pdescountry varchar(32),
			pdeszipcode char(8),
			pdesdistrict varchar(32)
		*/
		$results = $sql->select("CALL sp_addresses_save(:pidaddress, :pidperson, :pdesaddress, :desnumber, :pdescomplement, :pdescity, :pdesstate, :pdescountry, :pdeszipcode, :pdesdistrict)", array(
			":pidaddress"=>$this->getidaddress(),
			":pidperson"=>$this->getidperson(),
			":pdesaddress"=>utf8_decode($this->getdesaddress()),
			":desnumber"=>$this->getdesnumber(),
			":pdescomplement"=>utf8_decode($this->getdescomplement()),
			":pdescity"=>utf8_decode($this->getdescity()),
			":pdesstate"=>utf8_decode($this->getdesstate()),
			":pdescountry"=>utf8_decode($this->getdescountry()),
			":pdeszipcode"=>$this->getdeszipcode(),
			":pdesdistrict"=>$this->getdesdistrict()
		));


		if (count($results) > 0 ) {
			
			$this->setData($results[0]);
		}
	}

	public static function setMsgError($msg){

		$_SESSION[Address::SESSION_ERROR] = $msg;

	}

	public static function getMsgError(){

		$msg = (isset($_SESSION[Address::SESSION_ERROR])) ? $_SESSION[Address::SESSION_ERROR] : "";

		Address::clearMsgError();

		return $msg;

	}

	public static function clearMsgError(){

		$_SESSION[Address::SESSION_ERROR] = NULL;

	}
	
}
?>
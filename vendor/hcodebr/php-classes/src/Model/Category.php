<?php 

namespace Hcode\DB;
namespace Hcode\Model;


use Hcode\DB\Sql;
use Hcode\Model;

class Category extends Model{


	
	public static function listAll(){

		$sql = new Sql();

		return $results = 
		$sql->select("SELECT * FROM tb_categories  Order by descategory");
	}

	public function save() {

		$sql = new Sql();

		$results = $sql->select("CALL sp_categories_save(:idcategory, :descategory)", array(
			":idcategory"=>$this->getidcategory(),
			":descategory"=>$this->getdescategory(),
		));


		$this->setData($results[0]);
		Category::updateFile();
	}

	public function get($idcategory) {

		$sql = new Sql();

		$results = $sql->select("SELECT * FROM tb_categories WHERE idcategory = :id;", array(
			":id"=>$idcategory
		));

		$data = $results[0];

		$this->setData($data);

	}

	public function delete() {

		$sql = new Sql();

		$sql->query("DELETE FROM tb_categories WHERE (idcategory = :id)", array(
			":id"=>$this->getidcategory()
		));

		Category::updateFile();
	}

	public static function updateFile(){

		
		$categories = Category::listAll();
		$html = [];
		foreach ($categories as $row) {
			array_push($html, '<li><a href="/categories/'.$row["idcategory"].'">'.$row["descategory"].'</a></li>');
		}

		file_put_contents($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "categories-menu.html", implode('', $html));
	}


	public function getProducts($related = true){

		$sql = new Sql();

		if ($related) {
			return $results = $sql->select("SELECT * FROM tb_products WHERE idproduct IN (
				SELECT A.idproduct FROM tb_products AS A inner join tb_productscategories as B USING(idproduct)WHERE B.idcategory = :idcategory)",array(
					":idcategory"=> $this->getidcategory()
				));
		}else{
			return $results = $sql->select("SELECT * FROM tb_products WHERE idproduct NOT IN (
				SELECT A.idproduct FROM tb_products AS A inner join tb_productscategories as B USING(idproduct)WHERE B.idcategory = :idcategory)",array(
					":idcategory"=> $this->getidcategory()
				));
		}
		

	}

	public function getProductsPage($page = 1,$itensPerPage = 4){

		$start = ($page - 1) * $itensPerPage;

		$sql = new Sql();
		$results = $sql->select("SELECT SQL_CALC_FOUND_ROWS * FROM tb_products as a
			INNER JOIN tb_productscategories as b ON a.idproduct = b.idproduct
			INNER JOIN tb_categories as c ON c.idcategory = b.idcategory
			WHERE c.idcategory = :idcategory
			LIMIT $start,$itensPerPage",array(
				":idcategory"=>$this->getidcategory()
			));

		$resultTotal = $sql->select("SELECT FOUND_ROWS() as nrtotal");

		return [
			"data" => Product::checkList($results),
			"total"=> (int)$resultTotal[0]["nrtotal"],
			"pages"=>ceil($resultTotal[0]["nrtotal"]/$itensPerPage)
		];


	}


	public function addProduct(Product $product){

		$sql = new Sql();

		$sql->query("INSERT INTO tb_productscategories (idcategory,idproduct) VALUES(:idcategory,:idproduct)", array(
			":idcategory"=>$this->getidcategory(),
			":idproduct"=>$product->getidproduct()
		));


	}


	public function removeProduct(Product $product){

		$sql = new Sql();

		$sql->query("DELETE FROM tb_productscategories WHERE (idcategory = :idcategory and idproduct = :idproduct)", array(
			":idcategory"=>$this->getidcategory(),
			":idproduct"=>$product->getidproduct()
		));


	}

	public static function getPage($page = 1,$itensPerPage = 10){

		$start = ($page - 1) * $itensPerPage;

		$sql = new Sql();
		$results = $sql->select("SELECT SQL_CALC_FOUND_ROWS * 
			FROM tb_categories  Order by descategory
			LIMIT $start,$itensPerPage");

		$resultTotal = $sql->select("SELECT FOUND_ROWS() as nrtotal");

		return [
			"data" => $results,
			"total"=> (int)$resultTotal[0]["nrtotal"],
			"pages"=>ceil($resultTotal[0]["nrtotal"]/$itensPerPage)
		];


	}
	public static function getPageSearch($search,$page = 1,$itensPerPage = 10){

		$start = ($page - 1) * $itensPerPage;

		$sql = new Sql();
		$results = $sql->select("SELECT SQL_CALC_FOUND_ROWS * 
			FROM tb_categories  
			WHERE descategory like :search
			ORDER BY descategory
			LIMIT $start,$itensPerPage",array(
				":search" => '%' .$search . '%'
			));

		$resultTotal = $sql->select("SELECT FOUND_ROWS() as nrtotal");

		return [
			"data" => $results,
			"total"=> (int)$resultTotal[0]["nrtotal"],
			"pages"=>ceil($resultTotal[0]["nrtotal"]/$itensPerPage)
		];


	}

	
}
?>
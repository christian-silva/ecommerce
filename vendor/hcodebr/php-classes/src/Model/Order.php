<?php 

namespace Hcode\DB;
namespace Hcode\Model;


use Hcode\DB\Sql;
use Hcode\Model;
use Hcode\Model\Cart;

class Order extends Model{

	const ERROR = "OrderError";
	const ERROR_REGISTER = "OrderErrorRegister";
	const SUCCESS = "OrderSucesss";


	public function save(){

		$sql = new Sql();
		/*
			pidorder INT,
			pidcart int(11),
			piduser int(11),
			pidstatus int(11),
			pidaddress int(11),
			pvltotal decimal(10,2)
		*/
			$results = $sql->select("CALL sp_orders_save(:pidorder, :pidcart, :piduser, :pidstatus, :pidaddress, :pvltotal)", array(
				":pidorder"=>$this->getidorder(),
				":pidcart"=>$this->getidcart(),
				":piduser"=>$this->getiduser(),
				":pidstatus"=>$this->getidstatus(),
				":pidaddress"=>$this->getidaddress(),
				":pvltotal"=>$this->getvltotal()
			));


			if (count($results) > 0 ) {

				$this->setData($results[0]);
			}
		}

		public function get($idorder) {

			$sql = new Sql();

			$results = $sql->select("SELECT * FROM tb_orders as a 
				INNER JOIN tb_ordersstatus as b USING(idstatus) 
				INNER JOIN tb_carts as c USING(idcart) 
				INNER JOIN tb_users as d ON d.iduser = a.iduser
				INNER JOIN tb_addresses as e USING(idaddress) 
				INNER JOIN tb_persons as f ON f.idperson = d.idperson  
				WHERE a.idorder = :idorder", 
				array(
					":idorder"=>$idorder
				));

			if (count($results) > 0 ) {

				$this->setData($results[0]);
			}

		}
		public static function listAll(){

			$sql = new Sql();

			return $results = 
			$sql->select("SELECT * FROM tb_orders as a 
				INNER JOIN tb_ordersstatus as b USING(idstatus) 
				INNER JOIN tb_carts as c USING(idcart) 
				INNER JOIN tb_users as d ON d.iduser = a.iduser
				INNER JOIN tb_addresses as e USING(idaddress) 
				INNER JOIN tb_persons as f ON f.idperson = d.idperson  
				ORDER BY a.dtregister DESC");

		}

		public function delete() {

			$sql = new Sql();

			$sql->query("DELETE FROM tb_orders WHERE idorder = :idorder", array(
				":idorder"=>$this->getidorder()
			));


		}

		public function getCart(){

			$cart = new Cart();

			$cart->get((int) $this-> getidcart() );
			$cart->getCalculateTotal();
			return $cart;
		}


		public static function setError($msg)
		{

			$_SESSION[Order::ERROR] = $msg;

		}

		public static function getError()
		{

			$msg = (isset($_SESSION[Order::ERROR]) && $_SESSION[Order::ERROR]) ? $_SESSION[Order::ERROR] : '';

			Order::clearError();

			return $msg;

		}

		public static function clearError()
		{

			$_SESSION[Order::ERROR] = NULL;

		}

		public static function setSuccess($msg)
		{

			$_SESSION[Order::SUCCESS] = $msg;

		}

		public static function getSuccess()
		{

			$msg = (isset($_SESSION[Order::SUCCESS]) && $_SESSION[Order::SUCCESS]) ? $_SESSION[Order::SUCCESS] : '';

			Order::clearSuccess();

			return $msg;

		}

		public static function clearSuccess()
		{

			$_SESSION[Order::SUCCESS] = NULL;

		}



		public static function getPage($page = 1,$itensPerPage = 10){

			$start = ($page - 1) * $itensPerPage;

			$sql = new Sql();
			$results = $sql->select("SELECT SQL_CALC_FOUND_ROWS * 
				FROM tb_orders as a 
				INNER JOIN tb_ordersstatus as b USING(idstatus) 
				INNER JOIN tb_carts as c USING(idcart) 
				INNER JOIN tb_users as d ON d.iduser = a.iduser
				INNER JOIN tb_addresses as e USING(idaddress) 
				INNER JOIN tb_persons as f ON f.idperson = d.idperson  
				ORDER BY a.dtregister DESC
				LIMIT $start,$itensPerPage");

			$resultTotal = $sql->select("SELECT FOUND_ROWS() as nrtotal");

			return [
				"data" => $results,
				"total"=> (int)$resultTotal[0]["nrtotal"],
				"pages"=>ceil($resultTotal[0]["nrtotal"]/$itensPerPage)
			];


		}
		public static function getPageSearch($search,$page = 1,$itensPerPage = 10){

			$start = ($page - 1) * $itensPerPage;

			$sql = new Sql();
			$results = $sql->select("SELECT SQL_CALC_FOUND_ROWS * 
				FROM tb_orders as a 
				INNER JOIN tb_ordersstatus as b USING(idstatus) 
				INNER JOIN tb_carts as c USING(idcart) 
				INNER JOIN tb_users as d ON d.iduser = a.iduser
				INNER JOIN tb_addresses as e USING(idaddress) 
				INNER JOIN tb_persons as f ON f.idperson = d.idperson  
				WHERE  f.desperson like :search OR b.desstatus like :search 
				ORDER BY a.dtregister DESC
				LIMIT $start,$itensPerPage",array(
					":search" => '%' .$search . '%'
				));

			$resultTotal = $sql->select("SELECT FOUND_ROWS() as nrtotal");

			return [
				"data" => $results,
				"total"=> (int)$resultTotal[0]["nrtotal"],
				"pages"=>ceil($resultTotal[0]["nrtotal"]/$itensPerPage)
			];


		}

	}
?>	
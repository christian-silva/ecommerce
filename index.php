<?php 

session_start();

require_once("vendor/autoload.php");

use Slim\Slim;

$app = new Slim();

$app->config('debug', true);

require_once("functions.php");

/* Home do site, site2, site teste e categoria*/
require_once("site.php");

/* Administração do site - login, logout e forgot*/
require_once("admin.php");

/* Administração de Usuários */
require_once("admin-users.php");

/* Administração das Categorias */
require_once("admin-categories.php");

/* Administração das Categorias */
require_once("admin-products.php");

/*  Testes de formulário */
require_once("admin-formulario.php");

/*  Testes de orders admin */
require_once("admin-orders.php");

$app->run();

?>
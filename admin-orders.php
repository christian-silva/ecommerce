<?php  


use Hcode\PageAdmin;
use Hcode\Model\User;
use Hcode\Model\Order;
use Hcode\Model\OrderStatus;


$app->get('/admin/orders', function() {

	User::verifyLogin();
	$search = (isset($_GET['search'])) ?  $_GET['search'] : "";

	$page = (isset($_GET["page"])) ? (int)$_GET["page"] : 1;


	if ($search !== "") {
		$pagination = Order::getPageSearch($search,$page);
	}else{

		$pagination = Order::getPage($page);
	}


	$pages = [];

	for ($i=0; $i < $pagination["pages"]; $i++) { 

		array_push($pages, [
			"href"=>"/admin/orders?" . http_build_query(array(
				'page'=>$i+1,
				'search' =>$search
			)),
			"text"=>$i+1
		]);
	}

	$page = new PageAdmin();

	$page->setTpl("orders",array(
		"orders"=>$pagination['data'],
		"search"=>$search,
		"pages"=>$pages
	));
	
});

$app->get('/admin/orders/:idorders/delete', function($idorders) {

	User::verifyLogin();

	$order = new Order();

	$order->get((int)$idorders);

	$order->delete();

	header("Location: /admin/orders");
	exit;
	
});

$app->get('/admin/orders/:idorders', function($idorders){

	User::verifyLogin();

	$order = new Order();

	$order->get((int)$idorders);

	$cart = $order->getCart();

	$page = new PageAdmin();

	$page ->setTpl("order", array(
		"order"=>$order->getValues(),
		'cart'=>$cart->getValues(),
		'products' => $cart->getProducts() 
	));

});

$app->get('/admin/orders/:idorders/status', function($idorders) {

	User::verifyLogin();

	$order = new Order();

	$order->get((int)$idorders);

	$page = new PageAdmin();

	$page ->setTpl("order-status", array(
		"order"=>$order->getValues(),
		"status"=>OrderStatus::listAll(),
		"msgError"=>Order::getError(),
		"msgSuccess"=>Order::getSuccess()
	));

	
});

$app->post('/admin/orders/:idorders/status', function($idorders) {

	User::verifyLogin();

	if(!isset($_POST['idstatus']) || !(int)$_POST['idstatus'] > 0){

		User::setError("Preencha a senha atual");
		header("Location: /admin/orders/" .$idorders ."/status");
		exit;
	}


	$order = new Order();

	$order->get((int)$idorders);

	$order->setidstatus((int)$_POST['idstatus']);

	$order->save();

	Order::setSuccess("Status alterado com sucesso."); 

	header("Location: /admin/orders/" .$idorders ."/status");
	exit;

});



?>
<?php if(!class_exists('Rain\Tpl')){exit;}?>    <!--================================= FOOTER START =============================================-->
    <section class="container-fluid footer-section-space footer-bg">
        <div class="container white-text">
            <div class="row">
                <!--==========  COLUMN-1 ===============-->
                <div class="col-md-6 col-sm-12 col-xs-12 common-res-bottom common-res-bottom-1">
                    <h3 class="left posts-heading-bottom">latest posts</h3>
                    <div class="distab posts-bottom">
                        <div class="left distab-cell-middle">
                            <a href="#"><img src="/res/site2/images/100x100x1.png" alt="icon" />
                            </a>
                        </div>
                        <div class="left distab-cell-middle what-left-pad">
                            <p class="left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pharetra efficitur diam, vel sagittis ipsum. Curabitur eleifend, risus id sollicitudin</p>
                        </div>
                    </div>

                    <div class="distab posts-bottom">
                        <div class="left distab-cell-middle">
                            <a href="#"><img src="/res/site2/images/100x100x2.png" alt="icon" />
                            </a>
                        </div>
                        <div class="left distab-cell-middle what-left-pad">
                            <p class="left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pharetra efficitur diam, vel sagittis ipsum. Curabitur eleifend, risus id sollicitudin</p>
                        </div>
                    </div>
                </div>

                <!--==========  COLUMN-2 ===============-->
                <div class="col-md-2 col-sm-6 col-xs-6 common-full res-footer-links-bottom">
                    <h3 class="left posts-heading-bottom">important links</h3>
                    <div class="left">
                        <ul class="footer-list-bk footer-list-bottom no-padding no-margin">
                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Help Desk</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Our Services</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Our Features</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Support</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Contact Us</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="/res/site2/images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">About Us</a></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--==========  COLUMN-3 ===============-->
                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <h3 class="left posts-heading-bottom">contact us</h3>
                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="/res/site2/images/32x32x1.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls">47, Park Town, New York, USA</p>
                        </div>
                    </div>

                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="/res/site2/images/32x32x2.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls"><a href="#">mailid@domain.com</a>
                            </p>
                        </div>
                    </div>

                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="/res/site2/images/32x32x3.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls">+012-345-6789 </p>
                        </div>
                    </div>

                    <div class="left">
                        <ul class="no-padding no-margin footer-icon footer-left-pad">
                            <li>
                                <a href="#">
                                    <img src="/res/site2/images/48x48x1.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="/res/site2/images/48x48x2.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="/res/site2/images/48x48x3.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="/res/site2/images/48x48x4.png" alt="icon" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-br footer-br-bottom"></div>
            <p class="center ls">&copy; 2017, All Rights Reserved</p>
        </div>
    </section>
    <!--================================= FOOTER END =============================================-->
    <!-- JQUERY LIBRARY -->
    <script type="text/javascript" src="/res/site2/js/vendor/jquery.min.js"></script>
    <!-- BOOTSTRAP -->
    <script type="text/javascript" src="/res/site2/js/vendor/bootstrap.min.js"></script>

    <!-- SUBSCRIBE MAILCHIMP -->
    <script type="text/javascript" src="/res/site2/js/vendor/subscribe/subscribe_validate.js"></script>

    <!-- VALIDATION  -->
    <script type="text/javascript" src="/res/site2/js/vendor/validate/jquery.validate.min.js"></script>


    <!-- SLIDER JS FILES -->
    <script type="text/javascript" src="/res/site2/js/vendor/slider/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/res/site2/js/vendor/slider/carousel.js"></script>

    <!-- SUBSCRIBE MAILCHIMP -->
    <script type="text/javascript" src="/res/site2/js/vendor/subscribe/subscribe_validate.js"></script>

    <!-- IMAGE OVERLAY JS FILE -->
    <script type="text/javascript" src="/res/site2/js/vendor/img-overlay/modernizr.js"></script>
    <script type="text/javascript" src="/res/site2/js/vendor/img-overlay/overlay.js"></script>
    <!-- LIGHT BOX GALLERY -->
    <script type="text/javascript" src="/res/site2/js/vendor/lightbox/ekko-lightbox.js"></script>
    <script type="text/javascript" src="/res/site2/js/vendor/lightbox/lightbox.js"></script>

    <!-- VIDEO -->
    <script type="text/javascript" src="/res/site2/js/vendor/video/video.js"></script>

    <!-- COUNTER JS FILES -->
    <script type="text/javascript" src="/res/site2/js/vendor/counter/counter-lib.js"></script>
    <script type="text/javascript" src="/res/site2/js/vendor/counter/jquery.counterup.min.js"></script>

    <!-- PHONE_NUMBER VALIDATION JS -->
    <script type="text/javascript" src="/res/site2/js/vendor/phone_number/phone_number.js"></script>


    <!-- THEME JS -->
    <script type="text/javascript" src="/res/site2/js/custom/custom.js"></script>

</body>

</html>